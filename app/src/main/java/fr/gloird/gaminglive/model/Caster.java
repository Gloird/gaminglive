package fr.gloird.gaminglive.model;

/**
 * Created by nicolas on 22/12/2014.
 */
public class Caster {
    private int audiance;
    private String id;
    private boolean onaire;
    private String title;
    private String description;
    private String url;
    private String kiwi;

    public Caster() {
        this.audiance = 0;
        this.id = "";
        this.onaire = true;
        this.title = "";
        this.description = "";
        this.url = "";
        this.kiwi = "";

    }

    ;

    public Caster(int audiance, String id, boolean onaire, String title, String description, String url, String kiwi) {
        this.audiance = audiance;
        this.id = id;
        this.onaire = onaire;
        this.title = title;
        this.description = description;
        this.url = url;
        this.kiwi = kiwi;
    }

    public int getAudiance() {
        return audiance;
    }

    public void setAudiance(int audiance) {
        this.audiance = audiance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isOnaire() {
        return onaire;
    }

    public void setOnaire(boolean onaire) {
        this.onaire = onaire;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKiwi() {
        return kiwi;
    }

    public void setKiwi(String kiwi) {
        this.kiwi = kiwi;
    }

    @Override
    public String toString() {
        return "Caster{" +
                "audiance=" + audiance +
                ", id='" + id + '\'' +
                ", onaire=" + onaire +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", kiwi='" + kiwi + '\'' +
                '}';
    }
}
