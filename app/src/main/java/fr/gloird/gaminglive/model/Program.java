package fr.gloird.gaminglive.model;

/**
 * Created by nicolas on 02/01/2015.
 */
public class Program {
    private String heure;
    private String desc;
    private String titre;

    public Program(String heure, String desc, String titre) {
        this.heure = heure;
        this.desc = desc;
        this.titre = titre;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }
}
