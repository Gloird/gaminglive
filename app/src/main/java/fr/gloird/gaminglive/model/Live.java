package fr.gloird.gaminglive.model;

import java.util.List;

/**
 * Created by nicolas on 13/12/2014.
 */
public class Live {

    private Program program;
    private String titre;
    private String viewers;
    private String description;
    private String title;
    private String id_contenu;
    private String url;
    private String kiwi;

    public Live(Program program, String titre, String viewers, String description, String title, String id_contenu, String url, String kiwi) {
        this.program = program;
        this.titre = titre;
        this.viewers = viewers;
        this.description = description;
        this.title = title;
        this.id_contenu = id_contenu;
        this.url = url;
        this.kiwi = kiwi;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getViewers() {
        return viewers;
    }

    public void setViewers(String viewers) {
        this.viewers = viewers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId_contenu() {
        return id_contenu;
    }

    public void setId_contenu(String id_contenu) {
        this.id_contenu = id_contenu;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKiwi() {
        return kiwi;
    }

    public void setKiwi(String kiwi) {
        this.kiwi = kiwi;
    }


    public Boolean getIs_live() {

        return viewers != null;
    }
}
