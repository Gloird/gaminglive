package fr.gloird.gaminglive;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.purplebrain.adbuddiz.sdk.AdBuddiz;

import java.util.ArrayList;
import java.util.List;

import fr.gloird.gaminglive.model.Caster;
import fr.gloird.gaminglive.model.Live;

public class MainActivity extends ActionBarActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Context context = this;
    private SwipeRefreshLayout swipeLayout;

    public MainActivity() {
    }

    private ListView listViewData;
    private LiveAdapter liveAdapter;


    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private List<Live> listLive;
    private MenuAdapter adapter;


    private static final String PROPERTY_ID = "UA-53598706-5";
    public static int GENERAL_TRACKER = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //AdBuddiz
        AdBuddiz.setPublisherKey("f4906eba-f241-4ad8-ba1d-19e7356443a5");
        AdBuddiz.cacheAds(this); // this = current Activity
        if ((1 + (Math.random() * (1 - 2))) == 2) {// Anti flood Pub

            AdBuddiz.showAd(this);

        }
        //actionbar
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.action_bar, null);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#00aacb"));
        mActionBar.setBackgroundDrawable(colorDrawable);

        //Menu Lien

        TextView facebook = (TextView) findViewById(R.id.facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openURL("https://www.facebook.com/GamingLiveFR");
            }
        });

        TextView twitter = (TextView) findViewById(R.id.twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openURL("https://twitter.com/GamingLive");
            }
        });

        TextView apropos = (TextView) findViewById(R.id.apropos);
        apropos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AproposActivity.class);
                startActivityForResult(intent, 1);
            }
        });


        listViewData = (ListView) findViewById(R.id.listViewData);

        liveAdapter = new LiveAdapter(context, R.layout.list_live, new ArrayList<Live>());

        listViewData.setAdapter(liveAdapter);
        listViewData.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Bundle objetbunble = new Bundle();
                Live live = (Live) parent.getItemAtPosition(position);
                objetbunble.putString("id", live.getId_contenu());
                objetbunble.putString("Description", live.getDescription());
                objetbunble.putString("Title", live.getTitre());
                objetbunble.putString("Url", live.getUrl());
                objetbunble.putString("Audience", live.getViewers());
                objetbunble.putBoolean("Onair", live.getIs_live());
                objetbunble.putString("Kiwi", live.getKiwi());

                Log.v("test", live.getTitre() + " - " + live.getUrl() + " - " + live.getKiwi());
                Intent intent = new Intent(MainActivity.this, ChaineActivity.class);
                intent.putExtras(objetbunble);
                startActivityForResult(intent, 1);


            }

        });


        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(
                android.R.color.holo_blue_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light);


        LiveFetch fetcher = new LiveFetch(swipeLayout, listLive, liveAdapter);
        fetcher.execute();

        //menu
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.slider_list);
        adapter = new MenuAdapter(getApplicationContext(), new ArrayList<Caster>());
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new SlideitemListener());


        CasterFetch Cfetcher = new CasterFetch(new ArrayList<Caster>(), adapter);
        Cfetcher.execute();


        ImageButton Ib = (ImageButton) findViewById(R.id.imageButton);
        Ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });


    }

    public enum TrackerName {

        APP_TRACKER, // Tracker used only in this app.

        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.

        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.

    }

    class SlideitemListener implements ListView.OnItemClickListener {


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Caster m = (Caster) parent.getItemAtPosition(position);

            Caster caster = m;
            Bundle objetbunble = new Bundle();
            objetbunble.putString("id", caster.getId());
            objetbunble.putString("Description", caster.getDescription());
            objetbunble.putString("Title", caster.getTitle());
            objetbunble.putString("Url", caster.getUrl());
            objetbunble.putInt("Audience", caster.getAudiance());
            objetbunble.putBoolean("Onair", caster.isOnaire());
            objetbunble.putString("Kiwi", caster.getKiwi());

            Intent intent = new Intent(MainActivity.this, ChaineActivity.class);
            intent.putExtras(objetbunble);
            startActivityForResult(intent, 1);

        }
    }

    public void onRefresh() {

        liveAdapter = null;
        liveAdapter = new LiveAdapter(context, R.layout.list_live, new ArrayList<Live>());
        LiveFetch fetcher = new LiveFetch(swipeLayout, listLive, liveAdapter);
        fetcher.execute();


        adapter = null;
        adapter = new MenuAdapter(getApplicationContext(), new ArrayList<Caster>());

        CasterFetch Cfetcher = new CasterFetch(new ArrayList<Caster>(), adapter);
        Cfetcher.execute();

        swipeLayout.setRefreshing(false);
    }

    public void openURL(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


}
