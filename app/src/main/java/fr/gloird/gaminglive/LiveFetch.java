package fr.gloird.gaminglive;

import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.gloird.gaminglive.model.Live;

/**
 * Created by nicolas on 22/12/2014.
 */
public class LiveFetch extends AsyncTask<Void, Void, List<Live>> {
    private static final String TAG = "LiveFetch";
    public static final String SERVER_URL = "http://gloird.fr/json/gaminglive.php";
    SwipeRefreshLayout swipeLayout;
    private List<Live> listLive;
    private LiveAdapter liveAdapter;

    LiveFetch(SwipeRefreshLayout swipeLayout, List<Live> listLive, LiveAdapter liveAdapter) {
        this.swipeLayout = swipeLayout;
        this.listLive = listLive;
        this.liveAdapter = liveAdapter;
    }


    protected void onPreExecute() {
        super.onPreExecute();
        swipeLayout.setRefreshing(true);
    }

    @Override
    protected List<Live> doInBackground(Void... params) {

        List<Live> posts = new ArrayList<Live>();
        try {
            //Create an HTTP client
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(SERVER_URL);

            //Perform the request and check the status code
            HttpResponse response = client.execute(post);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();

                try {
                    //Read the server response and attempt to parse it as JSON
                    Reader reader = new InputStreamReader(content);

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                    Gson gson = gsonBuilder.create();
                    posts = Arrays.asList(gson.fromJson(reader, Live[].class));
                    content.close();
                } catch (Exception ex) {
                    Log.e(TAG, "Failed to parse JSON due to: " + ex);

                }
            } else {
                Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());

            }
        } catch (Exception ex) {
            Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);

        }
        return posts;
    }

    protected void onPostExecute(List<Live> result) {
        super.onPostExecute(result);
        listLive = result;
        for (Live l : result) {
            liveAdapter.add(l);
        }
        swipeLayout.setRefreshing(false);
    }

    public SwipeRefreshLayout getSwipeLayout() {
        return swipeLayout;
    }

    public void setSwipeLayout(SwipeRefreshLayout swipeLayout) {
        this.swipeLayout = swipeLayout;
    }

    public List<Live> getListLive() {
        return listLive;
    }

    public void setListLive(List<Live> listLive) {
        this.listLive = listLive;
    }

    public LiveAdapter getLiveAdapter() {
        return liveAdapter;
    }

    public void setLiveAdapter(LiveAdapter liveAdapter) {
        this.liveAdapter = liveAdapter;
    }
}