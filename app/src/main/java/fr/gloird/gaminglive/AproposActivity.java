package fr.gloird.gaminglive;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.gloird.gaminglive.model.Caster;


public class AproposActivity extends ActionBarActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private MenuAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apropos);

        //actionbar
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.action_bar, null);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#00aacb"));
        mActionBar.setBackgroundDrawable(colorDrawable);

        //Menu Lien

        TextView facebook = (TextView) findViewById(R.id.facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openURL("https://www.facebook.com/GamingLiveFR");
            }
        });

        TextView twitter = (TextView) findViewById(R.id.twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openURL("https://twitter.com/GamingLive");
            }
        });

        TextView home = (TextView) findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //menu
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.slider_list);
        adapter = new MenuAdapter(getApplicationContext(), new ArrayList<Caster>());
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new SlideitemListener());


        CasterFetch Cfetcher = new CasterFetch(new ArrayList<Caster>(), adapter);
        Cfetcher.execute();


        ImageButton Ib = (ImageButton) findViewById(R.id.imageButton);
        Ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        //https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S7244Y7YYTG3G
        TextView paypal = (TextView) findViewById(R.id.paypal);
        paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openURL("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S7244Y7YYTG3G");
            }
        });
    }


    public void openURL(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    class SlideitemListener implements ListView.OnItemClickListener {


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Caster m = (Caster) parent.getItemAtPosition(position);

            Caster caster = m;
            Bundle objetbunble = new Bundle();
            objetbunble.putString("id", caster.getId());
            objetbunble.putString("Description", caster.getDescription());
            objetbunble.putString("Title", caster.getTitle());
            objetbunble.putString("Url", caster.getUrl());
            objetbunble.putInt("Audience", caster.getAudiance());
            objetbunble.putBoolean("Onair", caster.isOnaire());
            objetbunble.putString("Kiwi", caster.getKiwi());

            Intent intent = new Intent(AproposActivity.this, ChaineActivity.class);
            intent.putExtras(objetbunble);
            startActivityForResult(intent, 1);

        }
    }
}
