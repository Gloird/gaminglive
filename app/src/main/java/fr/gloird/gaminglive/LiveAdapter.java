package fr.gloird.gaminglive;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.gloird.gaminglive.model.Live;

/**
 * Created by nicolas on 13/12/2014.
 */
public class LiveAdapter extends ArrayAdapter<Live> {
    Context context;
    int layoutResourceId;
    List<Live> objects;

    public LiveAdapter(Context context,int layoutResourceId, List<Live> objects) {
        super(context, layoutResourceId, objects);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        ViewHolder viewHolder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            viewHolder=  new ViewHolder();
            viewHolder.titre = (TextView) row.findViewById(R.id.tV_titre);
            viewHolder.desc = (TextView) row.findViewById(R.id.tV_desc);
            viewHolder.emmision = (TextView) row.findViewById(R.id.tV_emision);
            viewHolder.etat = (TextView) row.findViewById(R.id.tV_etat);
            viewHolder.iv_etat = (ImageView) row.findViewById(R.id.iV_etat1);
            viewHolder.iv_etat2 = (ImageView) row.findViewById(R.id.iV_etat2);

            row.setTag(viewHolder);

        }
        else
        {
            viewHolder = (ViewHolder)row.getTag();
        }
        Live live = objects.get(position);

        viewHolder.titre.setText(live.getTitre());

        if(live.getIs_live()){
            if(live.getProgram() != null) {
                viewHolder.desc.setText(live.getProgram().getDesc());
                viewHolder.emmision.setText(live.getProgram().getTitre());
            }else{
                viewHolder.emmision.setText("Emission en cours... ");
                viewHolder.desc.setText("");
            }
            viewHolder.etat.setText(live.getViewers());
            viewHolder.etat.setTextColor(Color.parseColor("#ee7c3c"));
            viewHolder.iv_etat.setImageResource(R.drawable.online);
            viewHolder.iv_etat2.setImageResource(R.drawable.online_2);
        }else{

            viewHolder.desc.setText("Il n'y a aucun live en ce moment.");
            viewHolder.emmision.setText("");
            viewHolder.etat.setText("Offline");
            viewHolder.etat.setTextColor(Color.parseColor("#6e797f"));
            viewHolder.iv_etat.setImageResource(R.drawable.offlline);
            viewHolder.iv_etat2.setImageResource(R.drawable.offline_2);
        }

        return row;
    }

    private class ViewHolder {
        TextView titre;
        TextView desc;
        TextView emmision;
        TextView etat;
        ImageView iv_etat;
        ImageView iv_etat2;
    }
}
