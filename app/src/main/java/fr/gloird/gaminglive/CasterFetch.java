package fr.gloird.gaminglive;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.gloird.gaminglive.model.Caster;

/**
 * Created by nicolas on 22/12/2014.
 */
public class CasterFetch extends AsyncTask<Void, Void, List<Caster>> {
    private static final String TAG = "CasterFetch";
    public static final String SERVER_URL = "http://gloird.fr/json/caster.php";
    private List<Caster> listLive;
    private MenuAdapter menuAdapter;

    CasterFetch(List<Caster> listLive, MenuAdapter menuAdapter) {
        this.listLive = listLive;
        this.menuAdapter = menuAdapter;
    }


    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<Caster> doInBackground(Void... params) {

        List<Caster> posts = new ArrayList<Caster>();
        try {
            //Create an HTTP client
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(SERVER_URL);

            //Perform the request and check the status code
            HttpResponse response = client.execute(post);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();

                try {
                    //Read the server response and attempt to parse it as JSON
                    Reader reader = new InputStreamReader(content);

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                    Gson gson = gsonBuilder.create();
                    posts = Arrays.asList(gson.fromJson(reader, Caster[].class));
                    content.close();
                } catch (Exception ex) {
                    Log.e(TAG, "Failed to parse JSON due to: " + ex);

                }
            } else {
                Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());

            }
        } catch (Exception ex) {
            Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);

        }
        return posts;
    }

    protected void onPostExecute(List<Caster> result) {
        super.onPostExecute(result);
        listLive = result;
        for (Caster l : result) {
            menuAdapter.add(l);
        }
    }

    public MenuAdapter getMenuAdapter() {
        return menuAdapter;
    }

    public void setLiveAdapter(MenuAdapter menuAdapter) {
        this.menuAdapter = menuAdapter;
    }

    public List<Caster> getListLive() {
        return listLive;
    }

    public void setListLive(List<Caster> listLive) {
        this.listLive = listLive;
    }
}