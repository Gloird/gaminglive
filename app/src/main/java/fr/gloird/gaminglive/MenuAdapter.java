package fr.gloird.gaminglive;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.gloird.gaminglive.model.Caster;

/**
 * Created by nicolas on 22/12/2014.
 */
public class MenuAdapter extends ArrayAdapter<Caster> {
    Context context;
    List<Caster> rowItem;

    MenuAdapter(Context context, List<Caster> rowItem) {
        super(context, R.layout.list_caster, rowItem);
        this.context = context;
        this.rowItem = rowItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_caster, null);
        }

        Caster row_pos;
        row_pos = rowItem.get(position);
        Caster live = row_pos;
        if (live.isOnaire()) {
            TextView tv_titre = (TextView) convertView.findViewById(R.id.tv_titre);
            tv_titre.setText(live.getTitle());
            ImageView iv1 = (ImageView) convertView.findViewById(R.id.iv_live);
            iv1.setImageResource(R.drawable.online);

        }
        // setting the image resource and title
        return convertView;
    }

}